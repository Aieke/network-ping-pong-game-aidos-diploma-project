using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;

public class NetworkPlayerSpawner : NetworkBehaviour
{
    private Dictionary<ulong, NetworkPlayer> _idToPlayerMap = new Dictionary<ulong, NetworkPlayer>();

    public GameObject[] playerPrefabs;
    public Vector3[] spawnPositions;

    public override void OnNetworkSpawn()
    {
        if (!IsServer)
            return;

        NetworkManager.OnClientConnectedCallback += OnClientConnected;
        base.OnNetworkSpawn();
    }

    public NetworkPlayer GetPlayerById(ulong id)
    {
        return _idToPlayerMap[id];
    }

    private void OnClientConnected(ulong playerId)
    {
        GameObject prefab = GetPrefabForPlayer(playerId);
        if (prefab == null)
            return;

        GameObject playerObject = Instantiate(prefab);
        NetworkObject networkObject = playerObject.GetComponent<NetworkObject>();

        playerObject.transform.position = GetSpawnPosition(playerId);

        networkObject.SpawnWithOwnership(playerId);
        _idToPlayerMap.Add(playerId, playerObject.GetComponent<NetworkPlayer>());
    }    

    private GameObject GetPrefabForPlayer(ulong playerId)
    {
        int index = (int)playerId;
        if (index >= 0 && index < playerPrefabs.Length)
            return playerPrefabs[index];

        return null;
    }

    private Vector3 GetSpawnPosition(ulong playerId)
    {
        int index = (int)playerId;
        if (index >= 0 && index < spawnPositions.Length)
            return spawnPositions[index];

        return Vector3.zero;
    }

}
