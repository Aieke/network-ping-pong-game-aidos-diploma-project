using Unity.Netcode;
using UnityEngine;
using Unity.Netcode.Components;

public class NetworkPlayer : NetworkBehaviour
{
    public Rigidbody2D rb;
    private Ball _ball;

    public override void OnNetworkSpawn()
    {
        if (IsHost)
        {
            _ball = FindObjectOfType<Ball>(); 
        }
        base.OnNetworkSpawn();
    }

    private void Update()
    {
        if (IsOwner)
        {
            rb.velocity = new Vector2(0, Input.GetAxisRaw("Vertical"));
            transform.position += new Vector3(0, Input.GetAxisRaw("Vertical")) * Time.deltaTime;
        }

        if (NetworkManager.Singleton.IsServer && NetworkManager.Singleton.ConnectedClientsList.Count == 2)
        {
            if (_ball != null && !_ball.gameObject.activeSelf)
            {
                _ball.gameObject.SetActive(true);
                _ball.Launch();
            }
        }
        else
        {
            if (_ball != null && _ball.gameObject.activeSelf)
            {
                _ball.gameObject.SetActive(false);
                _ball.Reset();
            }
        }
    }

}
