using UnityEngine;
using Unity.Netcode;

public class Ball : NetworkBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public Vector2 startPosition;
    private NetworkVariable<Vector2> m_ballPosition = new NetworkVariable<Vector2>(Vector2.zero);

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
        if (IsHost && IsClient)
        {
            m_ballPosition.Value = startPosition;
            rb.velocity = Vector2.zero;
            Launch();
        }
    }

    public void Reset()
    {
        m_ballPosition.Value = startPosition;
        rb.velocity = Vector2.zero;
    }

    public void Launch()
    {
        if (IsOwner && IsClient)
        {
            float x = Random.Range(0, 2) == 0 ? -1 : 1;
            float y = Random.Range(0, 2) == 0 ? -1 : 1;
            rb.velocity = new Vector2(speed * x, speed * y);
        }
    }

    private void FixedUpdate()
    {
        if (IsOwner)
        {
            m_ballPosition.Value = rb.position;
        }
    }
}
