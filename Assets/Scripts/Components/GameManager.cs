using System;
using TMPro;
using Unity.Netcode;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    [Header("Ball")]
    public GameObject ball;

    [Header("Player 1")]
    public GameObject player1Paddle;

    [Header("Player 2")]
    public GameObject player2Paddle;

    [Header("Score UI")]
    public GameObject Player1Text;
    public GameObject Player2Text;

    private NetworkVariable<int> player1Score = new NetworkVariable<int>(0);
    private NetworkVariable<int> player2Score = new NetworkVariable<int>(0);

    [SerializeField]private NetworkPlayerSpawner _spawnManager;

    [SerializeField] private TextMeshProUGUI _winnerText;

    private static NetworkVariable<bool> _limitCount = new NetworkVariable<bool>();

    private Ball ballComponent;
    private Vector3 ballStartPosition;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        player1Score.OnValueChanged += OnPlayer1ScoreChanged;
        player2Score.OnValueChanged += OnPlayer2ScoreChanged;
    }

    private void Start()
    {
        _winnerText = GameObject.FindGameObjectWithTag("text").GetComponent<TextMeshProUGUI>();

        ballComponent = ball.GetComponent<Ball>();
        ballStartPosition = ball.transform.position;
    }

    private void OnDestroy()
    {
        player1Score.OnValueChanged -= OnPlayer1ScoreChanged;
        player2Score.OnValueChanged -= OnPlayer2ScoreChanged;
    }

    private void OnPlayer1ScoreChanged(int previousValue, int newValue)
    {
        Player1Text.GetComponent<TextMeshProUGUI>().text = newValue.ToString();
    }

    private void OnPlayer2ScoreChanged(int previousValue, int newValue)
    {
        Player2Text.GetComponent<TextMeshProUGUI>().text = newValue.ToString();
    }

    public void PlayerScored(int playerId)
    {
        if (_limitCount.Value)
            return;

        int score;
        if (playerId == 0)
        {
            score = player1Score.Value++;
        }
        else if (playerId == 1)
        {
            score = player2Score.Value++;
        }
        else
        {
            return;
        }

        if (score >= 9)
        {
            string winnerName = GetWinnerName(playerId);
            ShowWinnerNameClientRpc(winnerName);
            ballComponent.Reset();
        }
    }

    private string GetWinnerName(int playerId)
    {
        NetworkPlayer networkPlayer = _spawnManager.GetPlayerById((ulong)playerId);
        if (networkPlayer != null)
        {
            return networkPlayer.gameObject.name;
        }
        return "";
    }

    [ClientRpc]
    private void ShowWinnerNameClientRpc(string winnerName)
    {
        _winnerText.text = "Winner:" + winnerName;
        _limitCount.Value = true;
    }
}
