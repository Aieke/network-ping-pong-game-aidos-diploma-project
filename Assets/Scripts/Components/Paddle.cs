using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class Paddle : NetworkBehaviour
{
    public Rigidbody2D rb;
    public Vector3 startPosition;    

    private void Start()
    {
        startPosition = transform.position;
    }  

    public void Reset()
    {
        if (!IsServer) return;

        rb.velocity = Vector2.zero;
        transform.position = startPosition;
    }
}
