using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class MultiplayerMenu : MonoBehaviour
{
    [SerializeField] private Button _startHostButton;
    [SerializeField] private Button _startClientButton;
    [SerializeField] private GameObject _root;

    private void Awake()
    {
        _startHostButton.onClick.AddListener(() => StartMultiplayer(true));
        _startClientButton.onClick.AddListener(() => StartMultiplayer(false));
    }

    private void StartMultiplayer(bool isHost)
    {
        if(isHost)
        {
            NetworkManager.Singleton.StartHost();
        }
        else
        {
            NetworkManager.Singleton.StartClient();
        }
        
        Ball ball = FindObjectOfType<Ball>(); // ����� ������ ����
        ball.Launch();
        
            _root.SetActive(false);
    }
}
